from enum import Enum
from typing import Tuple
import pandas as pd
import numpy as np
import bisect
import math
from datetime import datetime, timedelta

class Gas:
    aqi_bp_str = ("Good", "Fair", "Unhealthy for Sensitive Groups", "Very Unhealthy", "Acutely Unhealthy", "Emergency")
    aqi_bp_default = (0,51,101,151,201,301,500)
    sampling_period = 20 # 20 seconds
    minimum_points_per_hour = 0.75*(60*60)/sampling_period
    
    def __init__(self, bp: Tuple[float|int], period: int, precision: int, hour_offset: int, scaling: int, aqi_bp = (0,51,101,151,201,301,500), aqi_bp_str = ("Good", "Fair", "Unhealthy for Sensitive Groups", "Very Unhealthy", "Acutely Unhealthy", "Emergency")):
        self.bp_thresh = bp
        self.period = period
        self.precision = precision
        self.bp_boundary_differential = math.pow(10, -self.precision)
        self.offset = timedelta(hours=hour_offset)
        self.scaling = scaling
        self.aqi_bp = aqi_bp
        self.aqi_bp_str = aqi_bp_str
        
    def check_hour_valid(self, conc: pd.Series):
        idx = conc != 0
        valid_vals = idx.sum()
        print(f"Valid vals: {valid_vals} %: {valid_vals/points_per_hour}")
        
    def get_aqi(self, conc_ave: float):
        aqi_idx = self.find_aqi(conc_ave)
        bp_low = self.bp_thresh[aqi_idx]
        bp_high = self.bp_thresh[aqi_idx+1]-self.bp_boundary_differential
        i_low = self.aqi_bp[aqi_idx]
        i_high = self.aqi_bp[aqi_idx+1]-1
        # print(f"{bp_low} {bp_high} {i_low} {i_high} ")
        i = ((i_high-i_low)/(bp_high-bp_low))*(conc_ave-bp_low)+i_low
        return i
    
    @classmethod
    def get_hours(cls, conc: pd.Series):
        return conc.index.hour
    
    def get_aqi_24hr(self, conc, gas, valid = None):
        samp = conc/self.scaling
        agg = self.get_aqi_timeseries(samp, gas, valid)
        agg_24hr = pd.DataFrame(columns=["conc_ave", "AQI", "AQI String"])
        
        # resample to 24-hr if needed
        if self.period == 24:
            agg_24hr["conc_ave"] = agg["conc_ave"]
            agg_24hr["valid_count"] = agg["valid_count"]
            agg_24hr["AQI String"] = agg["AQI String"]
            agg_24hr["AQI"] = agg_24hr["conc_ave"].apply(self.get_aqi).round(1)
            print(agg_24hr["conc_ave"])
            return agg_24hr
        
        agg_24hr["conc_ave"] = agg["conc_ave"].resample('24H').mean()
        agg_24hr["valid_count"] = agg["valid_count"].resample('24H').sum()
        print(agg_24hr["conc_ave"])
        agg_24hr["AQI"] = agg_24hr["conc_ave"].apply(self.get_aqi).round(1)
        agg_24hr["AQI String"] = agg_24hr["conc_ave"].apply(self.find_aqi_str)
        return agg_24hr
    
    def get_aqi_timeseries(self, conc: pd.Series, gas, valid = None):
        hourly_avg_df = self.get_aqi_1hr(conc, valid)
        hourly_avg = hourly_avg_df["conc"]
        hourly_valid = hourly_avg_df["valid_count"]
        idx_valid = hourly_valid >= self.minimum_points_per_hour
        hourly_avg_toclean = hourly_avg_df.copy()
        hourly_avg_toclean.loc[~idx_valid, "conc"] = np.nan
        # DEBUG STUFF
        hourly_csv_name = gas + "_" + hourly_avg_df.index[0].strftime('%Y%m%d')
        hourly_csv = hourly_csv_name + ".csv"
        hourly_cleaned = hourly_csv_name + "_cleaned.csv"
        hourly_avg_df.to_csv("debug/"+hourly_csv)
        hourly_avg_toclean.to_csv("debug/"+hourly_cleaned)
        
        agg = pd.DataFrame(columns=["conc_ave", "AQI", "AQI String", "valid_count"])
        
        if self.period == 1:
            agg["conc_ave"] = hourly_avg_toclean["conc"]
            agg["valid_count"] = hourly_avg_toclean["valid_count"]
        else:
            period_str = str(self.period) + 'H'
            agg["conc_ave"] = hourly_avg_toclean["conc"].resample(period_str, offset=self.offset).mean()
            agg["valid_count"] = hourly_avg_toclean["valid_count"].resample(period_str, offset=self.offset).sum()
        
        agg["AQI"] = agg["conc_ave"].apply(self.get_aqi)
        agg["AQI String"] = agg["conc_ave"].apply(self.find_aqi_str)
        
        return agg
    
    def get_aqi_1hr(self, conc: pd.Series, valid: pd.Series = None) -> pd.DataFrame:
        hourly_avg = conc.resample('H').mean()
        hourly_valid = valid.resample('H').sum()
        
        hourly_avg_df = pd.DataFrame({"conc": hourly_avg, "valid_count": hourly_valid})
        return hourly_avg_df
    
    def find_aqi(self, conc: float|int):
        # round conc based on precision
        conc_rounded = round(conc, self.precision)
        aqi_idx = min(bisect.bisect_right(self.bp_thresh, conc_rounded), len(self.bp_thresh)-1)
        return aqi_idx-1
    
    def find_aqi_str(self, conc: float|int):        
        return self.aqi_bp_str[self.find_aqi(conc)]
        
class GasEnum(Enum):
    NO2 = Gas((0, 0.65, 1.25, 1.65), 1, 2, 0, 1, (0, 201, 301, 500), ("Good", "Acutely Unhealthy", "Emergency"))
    O3_8hr = Gas((0, 0.065, 0.085, 0.105, 0.125, 0.375, np.inf), 8, 3, 6, 1)
    O3_1hr = Gas((0, 0, 0.125, 0.165, 0.205, 0.405, 0.505), 1, 3, 0, 1)
    CO = Gas((0, 4.5, 9.5, 12.5, 15.5, 30.5, 40.5), 8, 1, 0, 1)
    SO2 = Gas((0, 0.035, 0.145, 0.225, 0.305, 0.605, 0.805), 24, 3, 0, 1)
    PM25 = Gas((0, 25, 35, 45, 55, 90, np.inf), 24, 1, 0, 1)
    PM10 = Gas((0, 55, 155, 255, 355, 425, 505), 24, 0, 0, 1)
    
name_mapping = {
    'NO2': 'NO2',
    'O3_8hr': 'O3_8hr',
    'O3_1hr': 'O3_1hr',
    'O3': 'O3_8hr',
    'CO': 'CO',
    'SO2': 'SO2',
    'PM2.5': 'PM25',  # Mapping 'PM2.5' to 'PM25'
    'PM10': 'PM10'
}

def get_gas_enum(gas):
    try:
        gas_enum = name_mapping[gas]
        return GasEnum[gas_enum].value
    except:
        raise ValueError("Gas not defined")