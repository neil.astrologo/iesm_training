import pandas as pd
import os

def hello_world():
    print("Hello world!")

def date_parse(date_string):
    no_tz = date_string[0:-6]
    date_format = "%Y-%m-%d %H:%M:%S"
    return pd.to_datetime(no_tz, format=date_format)

def get_date(date_col):
    return date_col.dt.date

def parse_month(full_data_path):
    df_list = []
    files = [file for file in os.listdir(full_data_path) if file.endswith('.csv')]
    for file in files:
        print(file)
        full_file_path = full_data_path + "/" + file
        tmp_df = pd.read_csv(full_file_path, header=0, names=["created_at", "entry_id", "NO2", "O3", "CO", "SO2", "Temperature", "RH", "PM2.5", "PM10"])
        dates = tmp_df["created_at"]
        attempted_parse = dates.apply(date_parse)
        tmp_df["datetime"] = attempted_parse
        tmp_df.drop("created_at", axis=1,inplace=True)
        tmp_df = tmp_df.set_index("datetime")
        df_list.append(tmp_df)
        
    df = pd.concat(df_list)
    return df

def clean_df(df):
    gas_names = ("NO2", "O3", "CO", "SO2", "PM2.5", "PM10", "Temperature", "RH")
    
    for gas in gas_names:
        gas_valid_name = gas + "_valid"
        idx_valid = df[gas] != 0.0
        df[gas_valid_name] = idx_valid
        
    return df